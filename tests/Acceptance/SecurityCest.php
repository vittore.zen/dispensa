<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;


use App\Tests\AcceptanceTester;

final class SecurityCest
{
    public function seeAuthenticationPage(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('Accedi');
    }

    public function loginAsUser(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->fillField('_username', 'user1');
        $I->fillField('_password', 'pass-1234');
        $I->click('Accedi');
        $I->see('User 1');
        $I->click('User 1');
        $I->see('Esci');
    }




    public function userLogout(AcceptanceTester $I):void
    {
        $I->amOnPage('/');
        $I->fillField('_username', 'user1');
        $I->fillField('_password', 'pass-1234');
        $I->click('Accedi');

        $I->click('User 1');
        $I->click('Esci');
        $I->see('Accedi');
        $I->dontSee('Esci');
    }


}
