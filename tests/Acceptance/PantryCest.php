<?php

declare(strict_types=1);

namespace App\Tests\Acceptance;

use App\Entity\Pantry;
use App\Tests\AcceptanceTester;

final class PantryCest
{

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->fillField('_username', 'user1');
        $I->fillField('_password', 'pass-1234');
        $I->click('Accedi');
    }


    public function seePantriesIndex(AcceptanceTester $I):void
    {
    $I->click('Dispense');
        $I->canSeeLink('Armadio bianco');
        $I->canSeeLink('Aggiungi dispensa');
    }

    public function addANewPantry(AcceptanceTester $I):void
    {
        $I->click('Dispense');
        $I->click('Aggiungi dispensa');

        $I->fillField('Nome', 'Armadio giallo');
        $I->checkOption('Abilitato');
        $I->click('Salva');
        $I->haveInRepository(Pantry::class,[['name'=>'Armadio giallo'],['name'=>'Armadio bianco']]);
        $I->canSeeLink('Armadio bianco');
        $I->canSeeLink('Armadio giallo');


    }
    public function modifyAPantry(AcceptanceTester $I):void
    {
        $I->click('Dispense');
        $I->click('Armadio bianco');

        $I->fillField('Nome', 'Armadio verde');
        $I->click('Salva');
        $I->canSeeLink('Armadio verde');
        $I->dontSeeLink('Armadio giallo');

    }


    public function deleteAPantry(AcceptanceTester $I):void
    {
        $I->click('Dispense');
        $I->click('Armadio bianco');
        $I->click('Cancella');
        $I->canSeeInPopup('Sei sicuro di voler cancellare questa dispensa?');
        $I->acceptPopup();
        $I->dontSeeLink('Armadio bianco');

    }
}
