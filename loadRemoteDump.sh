#!/usr/bin/env bash

remote=vps.zen.pn.it

echo "dump del db"
ssh root@$remote 'cd /tmp;sudo -u postgres pg_dump  dbname=dispensa > /tmp/tmp.sql'
scp root@$remote:/tmp/tmp.sql tmp.sql
vagrant ssh-config >vagrant-ssh
echo "Riavvio postgresql locale"
ssh -F vagrant-ssh default "sudo /etc/init.d/postgresql restart"
ssh -F vagrant-ssh default "php /vagrant/bin/console doctrine:database:drop --force"
ssh -F vagrant-ssh default "php /vagrant/bin/console doctrine:database:create"
ssh -F vagrant-ssh default "sudo -u postgres psql -U postgres -d dispensa < /vagrant/tmp.sql"

rm tmp.sql
rm vagrant-ssh
