#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev -yqq
apt-get install libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev -yqq
apt-get install libgmp3-dev libpq-dev libsqlite3-dev libaspell-dev libonig-dev libonig-dev libzip-dev -yqq     
apt-get install libpcre3-dev libtidy-dev libsodium-dev librabbitmq-dev libssl-dev libnss3  libgconf-2-4 -yqq
apt-get install wget gnupg2 -yqq

docker-php-ext-install pdo pdo_pgsql mbstring sodium curl intl gd xml zip bz2 zip sockets
pecl install amqp
docker-php-ext-enable amqp

# Install Composer and project dependencies.
curl -sS https://getcomposer.org/installer | php
php composer.phar install

# Copy over testing configuration.
cp .env.test-ci .env
cp .env.test-ci .env.test
cp phpstan.neon-test.dist phpstan.neon
php bin/console cache:warmup --env=test

#install node
#apt-get update && curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y build-essential nodejs
#npm init -y
#npm install -g eslint --unsafe-perm=true --alow-root
#npm config set prefix ~/.local
#PATH=~/.local/bin/:$PATH



# Install chrome
# Add key
#curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
# Add repo
#echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list
#apt-get update -q -y
#apt-get install -y google-chrome-stable

#npm install chromedriver -g
#npm install https://gitlab.com/gitlab-org/gitlab-selenium-server.git -g
# The `&` at the end causes it to run in the background and not block the following commands

#nohup chromedriver --port=4444 --url-base=wd/hub &
#nohup gitlab-selenium-server &
