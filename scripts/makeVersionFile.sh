#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE=$DIR/../config/version.yaml


version=`git describe --abbrev=0 --tags`
build=`git rev-list --count master`


echo "# This file is generated using script/makeVersionFile.sh" > $FILE
echo "twig:" >> $FILE
echo "    globals:" >> $FILE
echo "        version: $version" >> $FILE
echo "        build: $build" >> $FILE
