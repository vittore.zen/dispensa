<?php

namespace Deployer;

require 'recipe/common.php';
require 'recipe/symfony.php';


// Project name
set('application', 'boh');

// Project repository
set('repository', 'git@gitlab.com:vittore.zen/dispensa.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', [
    '.env'
]);
set('shared_dirs', [
    'var/log',
    'files-repository',
]);

// Writable dirs by web server
set('writable_dirs', [
    'var/cache',
    'var/log',
    'files-repository',
]);

set('composer_options', ' --verbose --prefer-dist --no-progress --no-interaction --no-scripts --no-dev --optimize-autoloader');
set('composer_options', ' --verbose --prefer-dist --no-progress --no-interaction --no-scripts  --optimize-autoloader');

set('http_user', 'www-data');
set('symfony_env', 'prod');

set('bin_dir', 'bin');
set('var_dir', 'app');
// Servers

set('keep_releases', 3);

// Hosts
set('remote_user', 'root');
host('vps.zen.pn.it')
    ->set('deploy_path', '/var/www/zenfamily.it/dispensa');
// Tasks

// Build yarn locally
task('deploy:build:assets', function (): void {
    runlocally('yarn install');
    runlocally('yarn encore production');
})->once()->desc('Install front-end assets');

before('deploy:symlink', 'deploy:build:assets');

// Upload assets
task('upload:assets', function (): void {
    upload(__DIR__ . '/public/assets/', '{{release_path}}/public/assets');
});

after('deploy:build:assets', 'upload:assets');


// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

//before('deploy:symlink', 'database:migrate');

after('deploy:symlink', 'supervisor:stop');
after('deploy:unlock', 'supervisor:start');

task('supervisor:stop', function () {
    run('service supervisor stop');
});

task('supervisor:start', function () {
    run('service supervisor start');
});