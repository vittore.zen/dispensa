<?php

namespace App\Security\Voters;

use App\Entity\House;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class HouseVoter extends Voter
{
    public const MANAGE = 'manage';

    protected function supports(string $attribute, $subject): bool
    {
        if (!in_array($attribute, [self::MANAGE])) {
            return false;
        }
        if (!$subject instanceof House) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $entry, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::MANAGE:
                return $this->canManage($entry, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canManage(House $entry, User $user, TokenInterface $token): bool
    {
        foreach ($entry->getOwners() as $owner) {
            if ($owner->getId() == $user->getId()) {
                return true;
            }
        }

        return false;
    }
}
