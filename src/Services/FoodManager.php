<?php

namespace App\Services;

use App\Entity\ItemToShop;
use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class FoodManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function removeFromShoppingList(Product $product, User $user): void
    {
        $items = $this->entityManager->getRepository(ItemToShop::class)
            ->findBy([
                'user' => $user,
                'product' => $product,
            ]);
        foreach ($items as $item) {
            $this->entityManager->remove($item);
        }
        $this->entityManager->flush();
    }
}
