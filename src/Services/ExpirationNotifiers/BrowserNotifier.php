<?php

namespace App\Services\ExpirationNotifiers;

use App\Entity\House;
use App\Entity\Product;
use App\Entity\ProductPresence;
use App\Entity\User;
use App\Services\UserSubscriptionManager;
use BenTools\WebPushBundle\Model\Message\PushNotification;
use BenTools\WebPushBundle\Sender\PushMessageSender;
use Doctrine\ORM\EntityManagerInterface;

class BrowserNotifier
{
    /**
     * @var ExpirationRetriever
     */
    private $expirationRetriever;

    /**
     * @var PushMessageSender
     */
    private $sender;

    /**
     * @var UserSubscriptionManager
     */
    private $userSubscriptionManager;

    public function __construct(ExpirationRetriever $expirationRetriever, PushMessageSender $sender, UserSubscriptionManager $userSubscriptionManager)
    {
        $this->expirationRetriever = $expirationRetriever;
        $this->sender = $sender;
        $this->userSubscriptionManager = $userSubscriptionManager;
    }

    public function ThreeDaysNotification(User $user): void
    {
        $expiredAlready = $this->expirationRetriever->getExpiredProducts($user, null);
            $expiredToday = $this->expirationRetriever->getExpiredProducts($user, 0);
            $expiredTomorrow = $this->expirationRetriever->getExpiredProducts($user, 1);
            $expiredTomorrowTomorrow = $this->expirationRetriever->getExpiredProducts($user, 2);
            $message = $this->composeMessage($expiredAlready,$expiredToday, $expiredTomorrow, $expiredTomorrowTomorrow);
            if ($message) {
                $this->sendNotification($user, $message);
            }
    }


    /**
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredAlready
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredToday
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredTomorrow
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredTomorrowTomorrow
     */
    private function composeMessage(array $expiredAlready,array $expiredToday, array $expiredTomorrow, array $expiredTomorrowTomorrow): ?string
    {
        if (
            0 == count($expiredAlready) &&
            0 == count($expiredToday) &&
            0 == count($expiredTomorrow) &&
            0 == count($expiredTomorrowTomorrow)
        ) {
            return null;
        }
        $message = '';
        if (count($expiredAlready) > 0) {
            $date = new \DateTime();
            $message .= 'Oggi, '.$date->format('Y-m-d')." sono già scaduti i seguenti prodotti:\r\n";
            $message .= $this->productLoop($expiredAlready);
        }
        if (count($expiredToday) > 0) {
            $date = new \DateTime();
            $message .= 'Oggi, '.$date->format('Y-m-d')." scadono i seguenti prodotti:\r\n";
            $message .= $this->productLoop($expiredToday);
        }
        if (count($expiredTomorrow) > 0) {
            $date = new \DateTime('+1 day');
            $message .= 'Domani, '.$date->format('Y-m-d')." scadono i seguenti prodotti:\r\n";
            $message .= $this->productLoop($expiredTomorrow);
        }
        if (count($expiredTomorrowTomorrow) > 0) {
            $date = new \DateTime('+2 days');
            $message .= 'Dopodomani, '.$date->format('Y-m-d')." scadono i seguenti prodotti:\r\n";
            $message .= $this->productLoop($expiredTomorrowTomorrow);
        }

        return $message;
    }

    private function productLoop(mixed $expiredList): string
    {
        $message = '';
        foreach ($expiredList as $houseId=>$list) {
            $house = $list['house'];
            $message.='*** '.$house->getName(). " ***\r\n";
            foreach ($list['expired'] as  $item) {
                $product = $item['product'];
                $quantity = $item['quantity'];
                $message .= '- ' . $product->__toString() . ' - ' . (
                    ($quantity > 1) ? "$quantity pezzi" : '1 pezzo'
                    ) . "\r\n";
            }
        }
        return $message;
    }

    private function sendNotification(User $user, string $message): void
    {
        $notification = new PushNotification('dispensa', [
            PushNotification::BODY => $message,
            PushNotification::ACTIONS => ['action' => 'read', 'title' => 'dispensa', 'link' => 'http://dispensa.zenfamily.it'],
        ]);
        $subscriptions = $user->getSubscriptions();
        $responses = $this->sender->push($notification->createMessage(), $subscriptions);
        foreach ($responses as $response) {
            if ($response->isExpired()) {
                $this->userSubscriptionManager->delete($response->getSubscription());
            }
        }
    }
}
