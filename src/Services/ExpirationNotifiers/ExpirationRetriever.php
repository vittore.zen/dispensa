<?php

namespace App\Services\ExpirationNotifiers;

use App\Entity\Product;
use App\Entity\House;
use App\Entity\ProductPresence;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class ExpirationRetriever
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @return  array<string, array<string, House|array<string, array<string,Product|int|null>>|null>>
     */
    public function getExpiredProducts(User $user, ?int $day): array
    {
        $qb = $this->entityManager->getRepository(ProductPresence::class)
            ->createQueryBuilder('productPresences')
            ->join('productPresences.pantry', 'pantry')
            ->join('pantry.house', 'house')
            ->join('house.owners', 'user')
            ->where('user = :user')
            ->setParameter('user', $user);


        if ($day!==null){
            $date = new \DateTime("+$day day");
            $qb->andWhere('productPresences.expiration = :date')
                ->setParameter('date', $date);
        } else {
            $date = new \DateTime();
            $qb->andWhere('productPresences.expiration < :date')
                ->setParameter('date', $date);
        }

        $productPresences = $qb->getQuery()->getResult();
        $list = [];
        foreach ($productPresences as $productPresence) {
            $houseId = (string) $productPresence->getPantry()->getHouse()->getId();
            $productId = (string) $productPresence->getProduct()->getId();
            if (!isset($list[$houseId])) {
                $list[$houseId]=['house'=>$productPresence->getPantry()->getHouse(),'expired'=>[]];
            }
            if (isset($list[$houseId]['expired'][$productId])) {
                $list[$houseId]['expired'][$productId]['quantity'] += $productPresence->getQuantity();
            } else {
                $list[$houseId]['expired'][$productId] = [
                    'product' => $productPresence->getProduct(),
                    'quantity' => $productPresence->getQuantity(),
                ];
            }
        }

        return $list;
    }

}
