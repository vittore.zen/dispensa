<?php

namespace App\Services\ExpirationNotifiers;

use App\Entity\House;
use App\Entity\Product;
use App\Entity\User;
use Pelago\Emogrifier\CssInliner;
use Psr\Container\ContainerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\WebpackEncoreBundle\Asset\EntrypointLookupInterface;
use Twig\Environment;

class EmailNotifier
{
    /**
     * @var ExpirationRetriever
     */
    private $expirationRetriever;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;


    public function __construct(ExpirationRetriever $expirationRetriever, MailerInterface $mailer, Environment $twig)
    {
        $this->expirationRetriever = $expirationRetriever;
        $this->mailer = $mailer;
        $this->twig = $twig;


    }

    public function ThreeDaysNotification(User $user): void
    {
        $expiredAlready = $this->expirationRetriever->getExpiredProducts($user, null);
        $expiredToday = $this->expirationRetriever->getExpiredProducts($user, 0);
        $expiredTomorrow = $this->expirationRetriever->getExpiredProducts($user, 1);
        $expiredTomorrowTomorrow = $this->expirationRetriever->getExpiredProducts($user, 2);
        $email = $this->composeMessage($user, $expiredAlready, $expiredToday, $expiredTomorrow, $expiredTomorrowTomorrow);
        if ($email) {
            $this->mailer->send($email);
        }
    }


    /**
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredAlready
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredToday
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredTomorrow
     * @param array<string, array<string, House|array<string, array<string,Product|int|null>>|null>> $expiredTomorrowTomorrow
     */
    private function composeMessage(User $user, array $expiredAlready, array $expiredToday, array $expiredTomorrow, array $expiredTomorrowTomorrow): ?Email
    {
        if (
            0 == count($expiredAlready) &&
            0 == count($expiredToday) &&
            0 == count($expiredTomorrow) &&
            0 == count($expiredTomorrowTomorrow)
        ) {
            return null;
        }

        $html = $this->twig->render('emails/notify_expired.html.twig', [
            'today' => new \DateTime(),
            'expiredAlready' => $expiredAlready,
            'expiredToday' => $expiredToday,
            'expiredTomorrow' => $expiredTomorrow,
            'expiredTomorrowTomorrow' => $expiredTomorrowTomorrow
        ]);

        $css = file_get_contents('https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css');

        // @phpstan-ignore-next-line
        $visualHtml = CssInliner::fromHtml($html)->inlineCss($css)->render();

        $email = (new Email())
            ->html($visualHtml)
            ->from('no-reply@zenfamily.it')
            ->to(new Address($user->getEmail()))
            //->to(new Address('vittore@zen.pn.it'))
            ->subject('dispensa');
        return $email;
    }


}
