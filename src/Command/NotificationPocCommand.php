<?php

namespace App\Command;

use App\Entity\User;
use BenTools\WebPushBundle\Model\Message\PushNotification;
use BenTools\WebPushBundle\Model\Subscription\UserSubscriptionManagerRegistry;
use BenTools\WebPushBundle\Sender\PushMessageSender;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotificationPocCommand extends Command
{
    protected static $defaultName = 'notification:poc';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PushMessageSender
     */
    private $sender;

    /**
     * @var UserSubscriptionManagerRegistry
     */
    private $userSubscriptionManager;

    public function __construct(EntityManagerInterface $entityManager, PushMessageSender $sender, UserSubscriptionManagerRegistry $userSubscriptionManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->sender = $sender;
        $this->userSubscriptionManager = $userSubscriptionManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = $this->entityManager->getRepository(User::class)->findOneByUsername('famiglia');

        $subscriptions = $this->userSubscriptionManager->findByUser($user);

        $notification = new PushNotification('Congratulations!', [
            PushNotification::BODY => 'Your order has been placed.',
        ]);
        $responses = $this->sender->push($notification->createMessage(), $subscriptions);
        foreach ($responses as $response) {
            if ($response->isExpired()) {
                $this->userSubscriptionManager->delete($response->getSubscription());
            }
        }

        return Command::SUCCESS;
    }
}
