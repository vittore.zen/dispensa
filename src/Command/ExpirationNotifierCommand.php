<?php

namespace App\Command;

use App\Entity\User;
use App\Services\ExpirationNotifiers\BrowserNotifier;
use App\Services\ExpirationNotifiers\EmailNotifier;
use App\Services\ExpirationNotifiers\ExpirationNotifier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExpirationNotifierCommand extends Command
{
    protected static $defaultName = 'notify-expiration';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var BrowserNotifier
     */
    private $browserNotifier;

    /**
     * @var EmailNotifier
     */
    private $emailNotifier;

    public function __construct(
        EntityManagerInterface $entityManager,BrowserNotifier $browserNotifier,
        EmailNotifier $emailNotifier
    )
    {
        $this->entityManager = $entityManager;
        $this->browserNotifier = $browserNotifier;
        $this->emailNotifier=$emailNotifier;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $users=$this->entityManager->getRepository(User::class
        )->findAll();
        foreach ($users as $user) {
            if (isset($user->getPreferences()['notificationOfExpiredFoodsViaBrowser']) &&
                $user->getPreferences()['notificationOfExpiredFoodsViaBrowser']){
                $this->browserNotifier->ThreeDaysNotification($user);
            }
            if (isset($user->getPreferences()['notificationOfExpiredFoodsViaEmail']) &&
                $user->getPreferences()['notificationOfExpiredFoodsViaEmail']){
                $this->emailNotifier->ThreeDaysNotification($user);
            }

        }

        return Command::SUCCESS;
    }
}
