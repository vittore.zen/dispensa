<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserResetPasswordCommand extends Command
{
    protected static $defaultName = 'user:password-reset';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Change password');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Username');
        $username = $io->ask('Username', null, function ($username) {
            if (empty($username)) {
                throw new \RuntimeException('Username cannot be empty.');
            }

            return $username;
        });
        $password = $io->askHidden('Password', function ($password) {
            if (empty($password)) {
                throw new \RuntimeException('Password cannot be empty.');
            }

            return $password;
        });

        $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(['username' => $username]);

        if ($user) {
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user, $password
            ));
            $this->entityManager->flush();

            $io->success('Password modificata.');

            return 0;
        } else {
            $io->warning('Utente non trovato.');

            return 1;
        }
    }
}
