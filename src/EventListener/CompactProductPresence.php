<?php

namespace App\EventListener;

use App\Entity\ProductPresence;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class CompactProductPresence implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postLoad
        ];
    }

    public function postLoad(LifecycleEventArgs $args): void
    {

        $entity = $args->getObject();
        if ($entity instanceof ProductPresence) {
            $this->compactProductPresence($entity);
        }
    }


    private function compactProductPresence($productPresence)
    {
        /*
        $pantry=$productPresence->getPantry();
        $parse=false;
        $list=[];
        foreach($pantry->getProductPresences() as $productPresence){
            $idx=(string)$productPresence->getProduct()->getId().'-'.$productPresence->getExpired()->format('Y-m-d');
            if (isset($list[$idx])){
                $parse=true;
            }
            $list[$idx][]=$productPresence;
        }
        if ($parse) {
            
        }


*/
    }
}