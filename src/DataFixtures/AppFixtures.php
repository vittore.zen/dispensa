<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\AccountCategory;
use App\Entity\Cash;
use App\Entity\House;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 5; ++$i) {
            $user[$i] = new User();
            $user[$i]->setUsername('user'.(string) $i);
            $user[$i]->setFirstname((string) $i);
            $user[$i]->setLastname('User');
            $user[$i]->setEmail('user'.(string) $i.'@test.us.com');
            $password = $this->hasher->hashPassword($user[$i], 'pass-1234');
            $user[$i]->setPassword($password);
            $manager->persist($user[$i]);
        }

        /*
                $accountIn = new Account();
                $accountIn->setName('account - in');
                $accountIn->setDirection('in');
                $accountIn->setIsDoubleCashesOperation(false);
                $accountIn->setOrder(1);
                $manager->persist($accountIn);


                $accountOut = new Account();
                $accountOut->setName('account - out');
                $accountOut->setDirection('out');
                $accountOut->setIsDoubleCashesOperation(false);
                $accountOut->setOrder(2);
                $manager->persist($accountOut);

                $accountBoth = new Account();
                $accountBoth->setName('account - both');
                $accountBoth->setDirection('both');
                $accountBoth->setIsDoubleCashesOperation(true);
                $accountBoth    ->setOrder(3);
                $manager->persist($accountBoth);

                $accountCategory = new AccountCategory();
                $accountCategory->setName('category');
                $accountCategory->setColor('white');
                $accountCategory->setOrder(1);
                $accountCategory->addAccount($accountIn);
                $manager->persist($accountCategory);

                for($i=1;$i<=3;$i++) {
                    $house[$i] = new House();
                    $house[$i]->setName('house '.(string)$i);
                    $house[$i]->setVisible(true);
                    $manager->persist($house[$i]);
                }

                $userFrate[1]->addHouse($house[1]);
                $userFrate[2]->addHouse($house[1]);
                $userFrate[2]->addHouse($house[2]);
                $userFrate[3]->addHouse($house[2]);
                $userFrate[4]->addHouse($house[3]);


                for($i=1;$i<=3;$i++) {
                    $cash[$i] = new Cash();
                    $cash[$i]->setName('cash '.(string)$i);
                    $cash[$i]->setVisible(true);
                    $cash[$i]->setStartingAmount(0);
                    $cash[$i]->setStartingDate(new \DateTime('2020-02-01'));
                    $manager->persist($cash[$i]);
                }

                $cash[1]->setHouse($house[1]);
                $cash[1]->addOwner($userFrate[2]);

                $cash[2]->setHouse($house[2]);
                $cash[2]->addOwner($userFrate[3]);
                $cash[3]->setHouse($house[2]);
                $cash[3]->addOwner($userFrate[3]);



                $manager->flush();
                */
        $manager->flush();
    }
}
