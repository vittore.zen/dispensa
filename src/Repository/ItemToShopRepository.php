<?php

namespace App\Repository;

use App\Entity\ItemToShop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ItemToShop>
 *
 * @method ItemToShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemToShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemToShop[]    findAll()
 * @method ItemToShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemToShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemToShop::class);
    }

   public function findAllByUser(User $user, mixed $filter): QueryBuilder
   {
       $qb = $this->createQueryBuilder('itemToShop');
       $qb->leftJoin('itemToShop.product', 'product')
           ->leftJoin('itemToShop.user', 'user')
           ->where('user.id = :me')
           ->setParameter('me', $user->getId());

       if (isset($filter['name'])) {
           $qb->andWhere('lower(product.name) like :name')->setParameter('name', '%'.strtolower($filter['name']).'%');
       }

       if (isset($filter['_sort'])) {
           $qb->orderBy('o.'.$filter['_sort']['field'], $filter['_sort']['direction']);
       }

       return $qb;
   }
}
