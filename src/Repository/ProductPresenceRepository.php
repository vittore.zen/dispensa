<?php

namespace App\Repository;

use App\Entity\ProductPresence;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProductPresence>
 *
 * @method ProductPresence|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPresence|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPresence[]    findAll()
 * @method ProductPresence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPresenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductPresence::class);
    }

   public function findAllByUserAndFilter(User $user, mixed $filter): QueryBuilder
   {
       $qb = $this->createQueryBuilder('productPresence');
       $qb->join('productPresence.pantry', 'pantry')
           ->join('productPresence.product', 'product')
           ->join('pantry.house', 'house')
           ->join('house.owners', 'user')
           ->where('user.id = :me')
           ->setParameter('me', $user->getId());

       if (isset($filter['name'])) {
           $qb->andWhere('lower(product.name) like :name')->setParameter('name', '%'.strtolower($filter['name']).'%');
       }
       if (isset($filter['barcode'])) {
           $qb->andWhere('lower(product.barcode) like :barcode')->setParameter('barcode', '%'.strtolower($filter['barcode']).'%');
       }

       if (isset($filter['house'])) {
           $qb->andWhere('house.id = :houseId')->setParameter('houseId', $filter['house']);
       }
       if (isset($filter['pantry'])) {
           $qb->andWhere('pantry.id = :pantryId')->setParameter('pantryId', $filter['pantry']);
       }

       if (isset($filter['_sort'])) {
           if (isset($filter['_sort']['field'])) {
               $qb->orderBy('productPresence.'.$filter['_sort']['field'], $filter['_sort']['direction']);
           }
       }

       return $qb;
   }
}
