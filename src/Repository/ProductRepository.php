<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return array<string, array<int, array<string, string>>>
     */
    public function findAllByNameForJsonSelect(string $queryString): array
    {
        $query = $this->createQueryBuilder('product')
            ->where('LOWER(product.name) like :queryString')
            ->setParameter('queryString', strtolower('%'.$queryString.'%'))
            ->getQuery();

        $products = $query->getResult();
        $list = [];
        foreach ($products as $product) {
            $list[] = ['id' => (string) $product->getId(), 'text' => $product->__toString()];
        }

        return ['results' => $list];
    }

    public function howMany(Product $product): int
    {
        $presences = $product->getProductPresences();
        $counter = 0;
        foreach ($presences as $presence) {
            $counter = $counter + $presence->getQuantity();
        }

        return $counter;
    }

    public function findAll()
    {
        return $this->findBy(array(), array('name' => 'ASC'));
    }
}
