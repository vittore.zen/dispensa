<?php

namespace App\Repository;

use App\Entity\Pantry;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Pantry>
 *
 * @method Pantry|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pantry|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pantry[]    findAll()
 * @method Pantry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PantryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pantry::class);
    }

    public function findAllByUser(User $user): QueryBuilder
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.house', 'h')
            ->leftJoin('h.owners', 'user')
            ->where('user.id = :uid')
            ->orderBy('h.name', 'ASC')
            ->orderBy('p.name', 'ASC')
            ->setParameter('uid', (string) $user->getid());
    }
}
