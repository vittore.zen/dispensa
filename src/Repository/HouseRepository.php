<?php

namespace App\Repository;

use App\Entity\House;
use App\Entity\Pantry;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Pantry>
 *
 * @method House|null find($id, $lockMode = null, $lockVersion = null)
 * @method House|null findOneBy(array $criteria, array $orderBy = null)
 * @method House[]    findAll()
 * @method House[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HouseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, House::class);
    }

    public function findAllByUser(User $user): QueryBuilder
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.owners', 'user')
            ->where('user.id = :uid')
            ->setParameter('uid', (string) $user->getid())
            ->orderBy('h.name', 'ASC');
    }
}
