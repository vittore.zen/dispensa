<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\Pantry;
use App\Entity\Product;
use App\Entity\ProductPresence;
use App\Repository\PantryRepository;
use App\Repository\BrandRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class Select2AjaxBrandType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;

    private $transformCallback;
    public function __construct(EntityManagerInterface $entityManager,
                                RouterInterface $router)
{
    $this->entityManager = $entityManager;
    $this->router = $router;
    $this->transformCallback = function ($stringOrDc) {
        if (is_string($stringOrDc)) return $stringOrDc;
        else                       return $stringOrDc->getId()->toString();
    };
}

    public function getParent()
{
    if($this->entityManager->getRepository(Brand::class)->count([]) > 50)
        return ChoiceType::class;
    else
        return EntityType::class;
}

    public function buildForm(FormBuilderInterface $builder, array $options)
{
    if($this->entityManager->getRepository(Brand::class)->count([]) > 50) {

        $builder->addModelTransformer(new CallbackTransformer(
            function ($dc) {
                /** @var $dc Brand|Brand[]|string|string[] */
                /** @return string|string[] */
                dump('model transform', $dc);
                if($dc === null) return '';

                if(is_array($dc)) {
                    return array_map($this->transformCallback, $dc);
                } else if($dc instanceof Collection) {
                    return $dc->map($this->transformCallback);
                } else {
                    return ($this->transformCallback)($dc);
                }
            },
            function ($id) {
                dump('model reversetransform', $id);
                if (is_string($id)) {
                    $dc = $this->entityManager->getRepository(Brand::class)->find($id);
                    if ($dc === null)
                        throw new TransformationFailedException("Konnte keine Datenkategorie mit ID $id finden");
                    dump($dc);
                    return $dc;
                } else {
                    $ret = [];
                    foreach($id as $i){
                        $dc = $this->entityManager->getRepository(Brand::class)->find($i);
                        if ($dc === null)
                            throw new TransformationFailedException("Konnte keine Datenkategorie mit ID $id finden");
                        $ret[] = $dc;
                    }
                    return $ret;
                }
            }
        ));
        $builder->resetViewTransformers();

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $dataId = $event->getData();
            dump('presubmit', $dataId, $event->getForm()->getConfig()->getOptions()['choices']);
            if(empty($dataId))
                return;

            $name = $event->getForm()->getName();

            if (is_array($dataId)) { // multiple-true-case
                if (!empty(array_diff($dataId, $event->getForm()->getConfig()->getOptions()['choices']))) {
                    $options = $event->getForm()->getParent()->get($name)->getConfig()->getOptions();
                    $options['choices'] = array_combine($dataId, $dataId);
                    $event->getForm()->getParent()->add($name, Select2AjaxBrandType::class, $options);
                    $event->getForm()->getParent()->get($name)->submit($dataId);
                    $event->stopPropagation();
                }
            } else { // multiple-false-case
                if($dataId instanceof Brand){
                    $dataId = $dataId->getId()->toString();
                    throw new \Exception('Hätte ich nicht erwartet, sollte string sein');
                }
                if (!in_array($dataId, $event->getForm()->getConfig()->getOptions()['choices'])) {
                    $options = $event->getForm()->getParent()->get($name)->getConfig()->getOptions();
                    $options['choices'] = [$dataId => $dataId];
                    $event->getForm()->getParent()->add($name, Select2AjaxBrandType::class, $options);
                    $event->getForm()->getParent()->get($name)->submit($dataId);
                    $event->stopPropagation();
                }
            }
        });
//            $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event){
//                dump("pre set data", $event->getData());
//            });
    } else {

    }
}

    public function configureOptions(OptionsResolver $resolver)
{
    if($this->entityManager->getRepository(Brand::class)->count([]) > 50) {
        $resolver->setDefaults([
            'attr' => [
                'data-ajax' => '1',
                'data-ajax-endpoint' => $this->router->generate('brand-json-for-select2')
            ],
            'choices' => function (Options $options) {
                $data = $options['data'];
                dump('data', $data);
                if($data !== null) {
                    if(is_array($data) || $data instanceof Collection){
                        $ret = [];
                        foreach ($data as $d) {
                            $ret[$d->getName()] = $d->getId()->toString();
                        }
                        dump($ret);
                        return $ret;
                    } else if ($data instanceof Brand){
                        return [$data->getName()=> $data->getId()->toString()];
                    } else {
                        throw new \InvalidArgumentException("Argument unerwartet.");
                    }
                } else {
                    return [];
                }
            }
        ]);
    } else {
        $resolver->setDefaults([
            'class' => Brand::class,
            'choice_label' => function ($cat, $key, $index) { return Brand::choiceLabel($cat);},
            'choices' => function (Options $options) {

                return $this->entityManager->getRepository(Brand::class)->getValidChildCategoryChoices($options['currentBrand']);
            }
        ]);
    }
}


}