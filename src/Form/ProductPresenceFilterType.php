<?php

namespace App\Form;

use App\Entity\House;
use App\Entity\Pantry;
use App\Repository\HouseRepository;
use App\Repository\PantryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductPresenceFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('barcode', TextType::class, ['required' => false])
            ->add('name', TextType::class, ['required' => false])
            ->add('house', EntityType::class, [
                'class' => House::class,
                'query_builder' => function (HouseRepository $houseRepository) use ($options) {
                    if ($options['user']) {
                        return $houseRepository->findAllByUser($options['user']);
                    }

                    return $houseRepository->createQueryBuilder('h')
                        ->orderBy('h.name', 'ASC');
                },
                'choice_label' => function (House $house) {
                    return $house->getName();
                },
                'required' => false,
            ])
            ->add('pantry', EntityType::class, [
                'class' => Pantry::class,
                'query_builder' => function (PantryRepository $pantryRepository) use ($options) {
                    if ($options['user']) {
                        return $pantryRepository->findAllByUser($options['user']);
                    }

                    return $pantryRepository->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => function (Pantry $pantry) {
                    return $pantry->getName();
                },
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'user' => null,
            'csrf_protection' => false,
            'method' => 'GET',
        ]);
    }
}
