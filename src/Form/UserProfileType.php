<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('language', ChoiceType::class, [
                'choices' => [
                    'Italiano' => 'it',
                    'English' => 'en',
                ],
                'required' => true,
                'getter' => function (User $user, FormInterface $form): ?string {
                    return $user->getPreferences()['language'] ?? null;
                },
                'setter' => function (User &$user, string $language, FormInterface $form): void {
                    $preferences = $user->getPreferences();
                    $preferences['language'] = $language;
                    $user->setPreferences($preferences);
                },
            ])
            ->add('notificationOfExpiredFoodsViaBrowser', CheckboxType::class, [
                'required' => false,
                'label_attr' => ['class' => 'checkbox-switch'],
                'getter' => function (User $user, FormInterface $form): bool {
                    return (bool) ($user->getPreferences()['notificationOfExpiredFoodsViaBrowser'] ?? false);
                },
                'setter' => function (User &$user, bool $value, FormInterface $form): void {
                    $preferences = $user->getPreferences();
                    $preferences['notificationOfExpiredFoodsViaBrowser'] = $value;
                    $user->setPreferences($preferences);
                },
            ])
            ->add('notificationOfExpiredFoodsViaEmail', CheckboxType::class, [
                'required' => false,
                'label_attr' => ['class' => 'checkbox-switch'],
                'getter' => function (User $user, FormInterface $form): bool {
                    return (bool) ($user->getPreferences()['notificationOfExpiredFoodsViaEmail'] ?? false);
                },
                'setter' => function (User &$user, bool $value, FormInterface $form): void {
                    $preferences = $user->getPreferences();
                    $preferences['notificationOfExpiredFoodsViaEmail'] = $value;
                    $user->setPreferences($preferences);
                },
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'user' => null,
            'data_class' => User::class,
        ]);
    }
}
