<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\Pantry;
use App\Entity\Product;
use App\Entity\ProductPresence;
use App\Repository\PantryRepository;
use App\Repository\BrandRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductPresenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('brand', EntityType::class, [
                'class' => Brand::class,
                'placeholder' => '',
                'query_builder' => function (BrandRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('b')
                        ->orderBy('b.name', 'ASC');
                },
            ])
        ;

        $formModifier = function (FormInterface $form, Brand $brand = null) {
            $products = null === $brand ? [] : $brand->getProducts();

            $form->add('product', EntityType::class, [
                'class' => Product::class,
                'placeholder' => '',
                'required'=>true,
                'choices' => $products,
            ]);
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                // this would be your entity, i.e. SportMeetup
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getBrand());
            }
        );

        $builder->get('brand')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $brand = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback function!
                $formModifier($event->getForm()->getParent(), $brand);
            }
        );

         $builder
            ->add('pantry', null, [
                'class' => Pantry::class,
                'query_builder' => function (PantryRepository $pantryRepository) use ($options) {
                    return $pantryRepository->findAllByUser($options['user']);
                },
                'choice_label' => function (Pantry $pantry) {
                    return $pantry->getHouse()->getName().' - '.$pantry->getName();
                },
                'by_reference' => false,
                'required'=>true,
            ])
            ->add('quantity', IntegerType::class)
            ->add('expiration', null, ['widget'=>'single_text','required' => false])
        ;





    }



    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'user' => null,
            'data_class' => ProductPresence::class,
        ]);
    }
}
