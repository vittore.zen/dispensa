<?php

namespace App\Form;

use App\Entity\House;
use App\Entity\Pantry;
use App\Repository\HouseRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PantryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('color', ColorType::class)
            ->add('house', EntityType::class, [
                'class' => House::class,
                'query_builder' => function (HouseRepository $pantryRepository) use ($options) {
                    if ($options['user']) {
                        return $pantryRepository->findAllByUser($options['user']);
                    }

                    return $pantryRepository->createQueryBuilder('c')
                        ->orderBy('c.name', 'ASC');
                },
                'choice_label' => function (House $house) {
                    return $house->getName();
                },
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'user' => null,
            'data_class' => Pantry::class,
        ]);
    }
}
