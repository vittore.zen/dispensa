<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BrandRepository")
 *
 * @Gedmo\Loggable
 */
class Brand
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", mappedBy="brands", )
     * @ORM\OrderBy({"name" = "ASC"})
     *
     * @var Collection<int,Product>
     */
    private $products;

    /**
     * One brand has many productPresences. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPresence",
     *     mappedBy="brand",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"},*
     *     )
     *
     * @var Collection<int,ProductPresence>
     */
    private $productPresences;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->products = new ArrayCollection();
    }

    public function __toString(): string
    {


        return $this->name??'';
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->addBrand($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            $product->removeBrand($this);
        }

        return $this;
    }


}
