<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HouseRepository")
 *
 * @Gedmo\Loggable
 */
class House
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $color;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="houses")
     *
     * @ORM\JoinTable(name="users_houses")
     *
     * @var Collection<int,User>
     */
    private $owners;

    /**
     * One house has many pantries. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Pantry",
     *     mappedBy="house",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"},
     *     )
     *
     * @var Collection<int,Pantry>
     */
    private $pantries;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->owners = new ArrayCollection();
        $this->pantries = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }

    public function addOwner(User $owner): self
    {
        if (!$this->owners->contains($owner)) {
            $this->owners->add($owner);
        }

        return $this;
    }

    public function removeOwner(User $owner): self
    {
        $this->owners->removeElement($owner);

        return $this;
    }

    /**
     * @return Collection<int, Pantry>
     */
    public function getPantries(): Collection
    {
        return $this->pantries;
    }

    public function addPantry(Pantry $pantry): self
    {
        if (!$this->pantries->contains($pantry)) {
            $this->pantries->add($pantry);
            $pantry->setHouse($this);
        }

        return $this;
    }

    public function removePantry(Pantry $pantry): self
    {
        if ($this->pantries->removeElement($pantry)) {
            // set the owning side to null (unless already changed)
            if ($pantry->getHouse() === $this) {
                $pantry->setHouse(null);
            }
        }

        return $this;
    }
}
