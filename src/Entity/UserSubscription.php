<?php

namespace App\Entity;

use BenTools\WebPushBundle\Model\Subscription\UserSubscriptionInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity()
 */
class UserSubscription implements UserSubscriptionInterface
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="string", unique=true)
     *
     * @var string
     */
    protected $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $subscriptionHash;

    /**
     * @var array<mixed>
     *
     * @ORM\Column(type="json")
     */
    private $subscription;

    /**
     * UserSubscription constructor.
     *
     * @param array<mixed> $subscription
     */
    public function __construct(User $user, string $subscriptionHash, array $subscription)
    {
        $this->id = (string) Uuid::v4();
        $this->user = $user;
        $this->subscriptionHash = $subscriptionHash;
        $this->subscription = $subscription;
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * {@inheritDoc}
     */
    public function getSubscriptionHash(): string
    {
        return $this->subscriptionHash;
    }

    /**
     * {@inheritDoc}
     */
    public function getEndpoint(): string
    {
        return $this->subscription['endpoint'];
    }

    /**
     * {@inheritDoc}
     */
    public function getPublicKey(): string
    {
        return $this->subscription['keys']['p256dh'];
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthToken(): string
    {
        return $this->subscription['keys']['auth'];
    }

    /**
     * Content-encoding (default: aesgcm).
     */
    public function getContentEncoding(): string
    {
        return $this->subscription['content-encoding'] ?? 'aesgcm';
    }

    public function setSubscriptionHash(string $subscriptionHash): self
    {
        $this->subscriptionHash = $subscriptionHash;

        return $this;
    }

    /**
     * @return array<mixed>
     */
    public function getSubscription(): array
    {
        return $this->subscription;
    }

    /**
     * @param array<mixed> $subscription
     *
     * @return $this
     */
    public function setSubscription(array $subscription): self
    {
        $this->subscription = $subscription;

        return $this;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
