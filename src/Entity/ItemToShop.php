<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemToShopRepository")
 *
 * @Gedmo\Loggable
 */
class ItemToShop
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * Many ItemToShop have one Product. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="itemsToShop")
     *
     * @var Product|null
     */
    private $product;

    /**
     * Many ItemToShop have one User. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="itemsToShop")
     *
     * @var User|null
     */
    private $user;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
