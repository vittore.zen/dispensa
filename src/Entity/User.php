<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 *
 * @ORM\Table(name="app_user")
 *
 * @UniqueEntity("email")
 *
 * @Gedmo\Loggable
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Gedmo\Versioned
     *
     * @var string
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     *
     * @Gedmo\Versioned
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=100)
     *
     * @Gedmo\Versioned
     */
    protected $password;

    /**
     * @var string|null
     */
    protected $plainPassword;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $passwordRequestToken;

    /**
     * @var array<int,string>
     *
     * @ORM\Column(type="array")
     *
     * @Gedmo\Versioned
     */
    private $roles = ['ROLE_USER'];

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $firstname;
    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @var string|null
     */
    private $lastname;

    /**
     * @ORM\Column(type="json", nullable=true)
     *
     * @Gedmo\Versioned
     *
     * @var array<string,string>|null
     */
    protected $preferences;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\House", mappedBy="owners")
     *
     * @var Collection<int,House>
     */
    private $houses;

    /**
     * One user has many itemsToShop. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ItemToShop",
     *     mappedBy="user",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     *     )
     *
     * @var Collection<int,ItemToShop>
     */
    private $itemsToShop;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $lastLogin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserSubscription", mappedBy="user")
     *
     * @var mixed
     */
    private $subscriptions;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->houses = new ArrayCollection();
        $this->itemsToShop = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): User
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPasswordRequestToken(): ?string
    {
        return $this->passwordRequestToken;
    }

    public function setPasswordRequestToken(?string $passwordRequestToken): User
    {
        $this->passwordRequestToken = $passwordRequestToken;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array<int,string> $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): User
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string The username
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getPreferences(): mixed
    {
        return $this->preferences;
    }

    /**
     * @return $this
     */
    public function setPreferences(mixed $preferences): self
    {
        $this->preferences = $preferences;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * @return Collection<int, ItemToShop>
     */
    public function getItemsToShop(): Collection
    {
        return $this->itemsToShop;
    }

    public function addItemsToShop(ItemToShop $itemsToShop): self
    {
        if (!$this->itemsToShop->contains($itemsToShop)) {
            $this->itemsToShop->add($itemsToShop);
            $itemsToShop->setUser($this);
        }

        return $this;
    }

    public function removeItemsToShop(ItemToShop $itemsToShop): self
    {
        if ($this->itemsToShop->removeElement($itemsToShop)) {
            // set the owning side to null (unless already changed)
            if ($itemsToShop->getUser() === $this) {
                $itemsToShop->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, UserSubscription>
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function addSubscription(UserSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions->add($subscription);
            $subscription->setUser($this);
        }

        return $this;
    }

    public function removeSubscription(UserSubscription $subscription): self
    {
        if ($this->subscriptions->removeElement($subscription)) {
            // set the owning side to null (unless already changed)
            if ($subscription->getUser() === $this) {
                $subscription->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, House>
     */
    public function getHouses(): Collection
    {
        return $this->houses;
    }

    public function addHouse(House $house): self
    {
        if (!$this->houses->contains($house)) {
            $this->houses->add($house);
            $house->addOwner($this);
        }

        return $this;
    }

    public function removeHouse(House $house): self
    {
        if ($this->houses->removeElement($house)) {
            $house->removeOwner($this);
        }

        return $this;
    }
}
