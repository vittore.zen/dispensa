<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @Gedmo\Loggable
 */
class Product
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    private $name;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Brand", inversedBy="products")
     * @ORM\OrderBy({"name" = "ASC"})
     * @ORM\JoinTable(name="brands_products")
     *
     * @var Collection<int,Brand>
     */
    private $brands;


    /**
     * One product has many productPresences. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPresence",
     *     mappedBy="product",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     *     )
     *
     * @var Collection<int,ProductPresence>
     */
    private $productPresences;

    /**
     * One product has many itemsToShop. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ItemToShop",
     *     mappedBy="product",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     *     )
     *
     * @var Collection<int,ItemToShop>
     */
    private Collection $itemsToShop;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->productPresences = new ArrayCollection();
        $this->itemsToShop = new ArrayCollection();
        $this->brands = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name??'';
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }



    /**
     * @return Collection<int, ProductPresence>
     */
    public function getProductPresences(): Collection
    {
        return $this->productPresences;
    }

    /**
     * @return Collection<int, ItemToShop>
     */
    public function getItemsToShop(): Collection
    {
        return $this->itemsToShop;
    }

    public function addItemsToShop(ItemToShop $itemsToShop): self
    {
        if (!$this->itemsToShop->contains($itemsToShop)) {
            $this->itemsToShop->add($itemsToShop);
            $itemsToShop->setProduct($this);
        }

        return $this;
    }

    public function removeItemsToShop(ItemToShop $itemsToShop): self
    {
        if ($this->itemsToShop->removeElement($itemsToShop)) {
            // set the owning side to null (unless already changed)
            if ($itemsToShop->getProduct() === $this) {
                $itemsToShop->setProduct(null);
            }
        }

        return $this;
    }

    public function addProductPresence(ProductPresence $productPresence): self
    {
        if (!$this->productPresences->contains($productPresence)) {
            $this->productPresences->add($productPresence);
            $productPresence->setProduct($this);
        }

        return $this;
    }

    public function removeProductPresence(ProductPresence $productPresence): self
    {
        if ($this->productPresences->removeElement($productPresence)) {
            // set the owning side to null (unless already changed)
            if ($productPresence->getProduct() === $this) {
                $productPresence->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Brand>
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    public function addBrand(Brand $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands->add($brand);
        }

        return $this;
    }

    public function removeBrand(Brand $brand): self
    {
        $this->brands->removeElement($brand);

        return $this;
    }
}
