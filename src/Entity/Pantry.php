<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PantryRepository")
 *
 * @Gedmo\Loggable
 */
class Pantry
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $color;

    /**
     * Many pantries have one house. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\House", inversedBy="pantries")
     *
     * @var House|null
     */
    private $house;

    /**
     * One pantry has many productPresences. This is the inverse side.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPresence",
     *     mappedBy="pantry",
     *     fetch="EXTRA_LAZY",
     *     orphanRemoval=true,
     *     cascade={"persist"}
     *     )
     *
     * @var Collection<int,ProductPresence>
     */
    private $productPeesences;

    public function __construct()
    {
        $this->id = Uuid::v4();
        $this->productPeesences = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, ProductPresence>
     */
    public function getProductPeesences(): Collection
    {
        return $this->productPeesences;
    }

    public function addProductPeesence(ProductPresence $productPeesence): self
    {
        if (!$this->productPeesences->contains($productPeesence)) {
            $this->productPeesences->add($productPeesence);
            $productPeesence->setPantry($this);
        }

        return $this;
    }

    public function removeProductPeesence(ProductPresence $productPeesence): self
    {
        if ($this->productPeesences->removeElement($productPeesence)) {
            // set the owning side to null (unless already changed)
            if ($productPeesence->getPantry() === $this) {
                $productPeesence->setPantry(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getHouse(): ?House
    {
        return $this->house;
    }

    public function setHouse(?House $house): self
    {
        $this->house = $house;

        return $this;
    }
}
