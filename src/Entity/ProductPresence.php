<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPresenceRepository")
 *
 * @Gedmo\Loggable
 */
class ProductPresence
{
    /**
     * @ORM\Id
     *
     * @ORM\Column(type="uuid", unique=true)
     *
     * @var Uuid
     */
    protected $id;


    /**
     * Many productPresences have one brand. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Brand", inversedBy="productPresences")
     *
     * @var Brand|null
     */
    private $brand;

    /**
     * Many productPresences have one product. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productPresences")
     *
     * @var Product|null
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $quantity;

    /**
     * Many pantries have one productPresences. This is the owning side.
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Pantry", inversedBy="productPresences")
     *
     * @var Pantry|null
     */
    private $pantry;

    /**
     * @ORM\Column(type="date",nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $expiration;

    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getExpiration(): ?\DateTimeInterface
    {
        return $this->expiration;
    }

    public function setExpiration(?\DateTimeInterface $expiration): self
    {
        $this->expiration = $expiration;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPantry(): ?Pantry
    {
        return $this->pantry;
    }

    public function setPantry(?Pantry $pantry): self
    {
        $this->pantry = $pantry;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }
}
