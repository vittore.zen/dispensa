<?php

namespace App\Controller;

use App\Entity\Pantry;
use App\Entity\Product;
use App\Entity\ProductPresence;
use App\Entity\User;
use App\Form\ProductPresenceFilterType;
use App\Form\ProductPresenceType;
use App\Repository\HouseRepository;
use App\Repository\PantryRepository;
use App\Repository\ProductPresenceRepository;
use App\Repository\ProductRepository;
use App\Services\FoodManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PUGX\FilterBundle\Filter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User|null getUser()
 */
#[Route('/product-presence')]
class ProductPresenceController extends AbstractController
{
    #[Route('/', name: 'product_presence_index', methods: ['GET'])]
    public function index(
        Request $request, PaginatorInterface $paginator, Filter $filter,
        ProductPresenceRepository $productPresenceRepository,
        HouseRepository $houseRepository,
        PantryRepository $pantryRepository): Response
    {
        $session = new Session();
        $session->set('return_route', 'product_presence_index');
        if (null !== $request->get('force_pantry') && (!isset($session->get('filter.product_presence')['pantry']) ||
            $session->get('filter.product_presence')['pantry'] != $request->get('force_pantry'))) {
            $session->set('filter.product_presence', ['pantry' => $request->get('force_pantry')]);
        }
        if (null !== $request->get('force_house') && (!isset($session->get('filter.product_presence')['house']) ||
            $session->get('filter.product_presence')['house'] != $request->get('force_house'))) {
            $session->set('filter.product_presence', ['house' => $request->get('force_house')]);
        }
        if (null !== $request->get('force_barcode') &&
            (!isset($session->get('filter.product_presence')['barcode']) || $session->get('filter.product_presence')['barcode'] != $request->get('force_barcode'))
        ) {
            $session->set('filter.product_presence', ['barcode' => $request->get('force_barcode')]);
        }
        if ($filter->saveFilter(ProductPresenceFilterType::class, 'product_presence')) {
            return $this->redirectToRoute('product_presence_index');
        }
        $page = $request->query->getInt('page', $session->get('product_presence', 1));
        $session->set('product_presence', $page);
        $entities =$productPresenceRepository->findAllByUserAndFilter($this->getUser(),
                $filter->filter('product_presence')
            )->getQuery()
            ->getResult();

        $pantries = $pantryRepository->findAllByUser($this->getUser())->getQuery()->getResult();

        $data = [];
        $data['name'] = isset($filter->filter('product_presence')['name']) ? $filter->filter('product_presence')['name'] : null;
        $data['barcode'] = isset($filter->filter('product_presence')['barcode']) ? $filter->filter('product_presence')['barcode'] : null;
        $data['house'] = null;
        if (isset($filter->filter('product_presence')['house'])) {
            $data['house'] = $houseRepository->find($filter->filter('product_presence')['house']);
        }

        $data['pantry'] = null;
        if (isset($filter->filter('product_presence')['pantry'])) {
            $data['pantry'] = $pantryRepository->find($filter->filter('product_presence')['pantry']);
        }

        $filterForm = $this->createForm('App\Form\ProductPresenceFilterType', $data, ['user' => $this->getUser()]);

        return $this->render('product_presence/index.html.twig', [
            'entities' => $entities,
            'form' => $filterForm->createView(),
            'pantries' => $pantries,
        ]);
    }

    #[Route('/sort/{field}/{direction}', name: 'product_presence_sort', requirements: ['direction' => 'ASC|DESC'], methods: [Request::METHOD_GET])]
    public function sort(Filter $filter, string $field, string $direction = 'ASC'): Response
    {
        $filter->sort('product_presence', $field, $direction);

        return $this->redirectToRoute('product_presence_index');
    }

    #[Route('/new', name: 'product_presence_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, FoodManager $foodManager): Response
    {
        $productPresence = new ProductPresence();
        $form = $this->createForm(ProductPresenceType::class, $productPresence, ['user' => $this->getUser()]);
        $form->add('afterAction', HiddenType::class, ['mapped' => false]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $actionAfter = $form['afterAction']->getData();
            $entityManager->persist($productPresence);
            $entityManager->flush();
            $foodManager->removeFromShoppingList($productPresence->getProduct(), $this->getUser());
            if ('add' == $actionAfter) {
                return $this->redirectToRoute('product_presence_new', [], Response::HTTP_SEE_OTHER);
            }
            $session = new Session();
            if ('homepage' == $session->get('return_route')) {
                return $this->redirectToRoute('homepage', [], Response::HTTP_SEE_OTHER);
            }

            return $this->redirectToRoute('product_presence_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('product_presence/new.html.twig', [
            'productPresence' => $productPresence,
            'form' => $form,
        ]);
    }

    #[Route('/remove/{productPresenceId}/{quantity}', name: 'product_presence_remove', methods: ['GET', 'POST'])]
    public function remove(Request $request, string $productPresenceId, string $quantity, EntityManagerInterface $entityManager): Response
    {
        $productPresence = $entityManager->find(ProductPresence::class, $productPresenceId);
        $this->denyAccessUnlessGranted('manage', $productPresence->getPantry());
        $product = $productPresence->getProduct();
        if ($productPresence->getQuantity() == (int) $quantity) {
            $entityManager->remove($productPresence);
            $entityManager->flush();
        } else {
            if ($productPresence->getQuantity() < (int) $quantity) {
                throw new \Exception('Error in quantity');
            }
            $productPresence->setQuantity($productPresence->getQuantity() - (int) $quantity);
            $entityManager->flush();
        }

        $howMany = $entityManager->getRepository(Product::class)->howMany($productPresence->getProduct());
        if ($howMany <= 1) {
            return $this->redirectToRoute('product_presence_product_is_the_last_or_almost', ['productId' => $product->getId(), 'remainder' => $howMany], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('product_presence_index', [], Response::HTTP_SEE_OTHER);
    }



    #[Route('/products-by-name/json', name: 'product_presence_product_by_name_json', methods: ['GET'])]
    public function productsByName(Request $request, ProductRepository $productRepository): JsonResponse
    {
        $searchString = $request->get('q', '');

        return new JsonResponse($productRepository->findAllByNameForJsonSelect($searchString));
    }

    #[Route('/isTheLastOrAlmost/{productId}/{remainder}', name: 'product_presence_product_is_the_last_or_almost', methods: ['GET'])]
    public function isTheLastOrAlmost(string $productId, string $remainder, ProductRepository $productRepository): Response
    {
        return $this->render('product_presence/question.html.twig', [
            'product' => $productRepository->find($productId),
            'remainder' => $remainder,
        ]);
    }

    #[Route('/move/{productPresenceId}/{quantity}/{pantryId}', name: 'product_presence_move', methods: ['GET', 'POST'])]
    public function move(Request $request, string $productPresenceId, string $quantity, string $pantryId, EntityManagerInterface $entityManager): Response
    {
        $productPresence = $entityManager->find(ProductPresence::class, $productPresenceId);
        $pantry = $entityManager->find(Pantry::class, $pantryId);
        $this->denyAccessUnlessGranted('manage', $productPresence->getPantry());
        $this->denyAccessUnlessGranted('manage', $pantry);
        if ($productPresence->getQuantity() == (int) $quantity) {
            $productPresence->setPantry($pantry);
        } else {
            if ($productPresence->getQuantity() < (int) $quantity) {
                throw new \Exception('Error in quantity');
            }
            $productPresence->setQuantity($productPresence->getQuantity() - (int) $quantity);

            $productPresence2 = new ProductPresence();
            $productPresence2->setProduct($productPresence->getProduct());
            $productPresence2->setPantry($pantry);
            $productPresence2->setQuantity((int) $quantity);
            $productPresence2->setExpiration($productPresence->getExpiration());
            $entityManager->persist($productPresence2);
        }
        $entityManager->flush();

        return $this->redirectToRoute('product_presence_index', [], Response::HTTP_SEE_OTHER);
    }
}
