<?php

namespace App\Controller;

use App\Entity\House;
use App\Entity\User;
use App\Form\HouseType;
use App\Repository\HouseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User|null getUser()
 */
#[Route('/house')]
class HouseController extends AbstractController
{
    #[Route('/', name: 'house_index', methods: ['GET'])]
    public function index(HouseRepository $houseRepository): Response
    {
        return $this->render('house/index.html.twig', [
            'houses' => $houseRepository->findAllByUser($this->getUser())->getQuery()->getResult(),
        ]);
    }

    #[Route('/new', name: 'house_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $house = new House();
        $form = $this->createForm(HouseType::class, $house);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $house->addOwner($this->getUser());
            $entityManager->persist($house);
            $entityManager->flush();

            return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('house/new.html.twig', [
            'house' => $house,
            'form' => $form,
        ]);
    }

    /*
        #[Route('/{id}/show', name: 'house_show', methods: ['GET'])]
        public function show(House $house): Response
        {
            $this->denyAccessUnlessGranted('manage', $house);
            return $this->render('house/show.html.twig', [
                'house' => $house,
            ]);
        }
    */
    #[Route('/{id}/edit', name: 'house_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, House $house, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('manage', $house);
        $form = $this->createForm(HouseType::class, $house);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('house/edit.html.twig', [
            'house' => $house,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'house_delete', methods: ['POST'])]
    public function delete(Request $request, House $house, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('manage', $house);
        if ($this->isCsrfTokenValid('delete'.$house->getId(), $request->request->get('_token'))) {
            $entityManager->remove($house);
            $entityManager->flush();
        }

        return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
    }
}
