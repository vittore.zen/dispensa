<?php

namespace App\Controller;

use App\Entity\ItemToShop;
use App\Entity\Product;
use App\Entity\User;
use App\Services\FoodManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method User|null getUser()
 */
class FastActionController extends AbstractController
{
    #[\Symfony\Component\Routing\Annotation\Route('/add-product-to-shopping-list/{productId}', name: 'add_product_to_shopping_list', methods: ['GET', 'POST'])]
    public function addProductToShoppingList(Request $request, string $productId, EntityManagerInterface $entityManager, FoodManager $foodManager): Response
    {
        $product = $entityManager->find(Product::class, $productId);
        $itemToShop = new ItemToShop();
        $itemToShop->setUser($this->getUser());
        $itemToShop->setProduct($product);
        $entityManager->persist($itemToShop);
        $entityManager->flush();

        return $this->redirectToRoute('homepage', [], Response::HTTP_SEE_OTHER);
    }
}
