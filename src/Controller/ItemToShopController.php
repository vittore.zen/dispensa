<?php

namespace App\Controller;

use App\Entity\ItemToShop;
use App\Entity\User;
use App\Form\ItemToShopFilterType;
use App\Form\ItemToShopType;
use App\Repository\ItemToShopRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PUGX\FilterBundle\Filter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User|null getUser()
 */
#[Route('/shopping-list')]
class ItemToShopController extends AbstractController
{
    #[Route('/', name: 'shopping_list_index', methods: ['GET'])]
    public function index(Request $request, PaginatorInterface $paginator, Filter $filter, ItemToShopRepository $itemToShopRepository): Response
    {
        $session = new Session();
        if ($filter->saveFilter(ItemToShopFilterType::class, 'item_to_shop')) {
            return $this->redirectToRoute('homepage');
        }
        $page = $request->query->getInt('page', $session->get('item_to_shop', 1));
        $session->set('item_to_shop', $page);
        $pagination = $paginator->paginate(
            $itemToShopRepository->findAllByUser($this->getUser(),
                $filter->filter('item_to_shop'))->getQuery(),
            $page, 10
        );

        return $this->render('shopping_list/index.html.twig', [
            'pagination' => $pagination,
            'form' => $filter->getFormView('item_to_shop'),
        ]);
    }

    #[Route('/new', name: 'shopping_list_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $itemToShop = new ItemToShop();
        $itemToShop->setUser($this->getUser());
        $form = $this->createForm(ItemToShopType::class, $itemToShop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($itemToShop);
            $entityManager->flush();

            return $this->redirectToRoute('shopping_list_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('shopping_list/new.html.twig', [
            'item_to_shop' => $itemToShop,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'shopping_list_delete', methods: ['GET'])]
    public function delete(Request $request, ItemToShop $itemToShop, EntityManagerInterface $entityManager): Response
    {
        if ($itemToShop->getUser()->getId() == $this->getUser()->getId()) {
            $entityManager->remove($itemToShop);
            $entityManager->flush();
        }

        return $this->redirectToRoute('shopping_list_index', [], Response::HTTP_SEE_OTHER);
    }
}
