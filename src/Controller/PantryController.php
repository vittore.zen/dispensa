<?php

namespace App\Controller;

use App\Entity\Pantry;
use App\Entity\User;
use App\Form\PantryType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @method User|null getUser()
 */
#[Route('/pantry')]
class PantryController extends AbstractController
{
    #[Route('/new', name: 'pantry_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $pantry = new Pantry();
        $form = $this->createForm(PantryType::class, $pantry, ['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $pantry->addOwner($this->getUser());
            $entityManager->persist($pantry);
            $entityManager->flush();

            return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pantry/new.html.twig', [
            'pantry' => $pantry,
            'form' => $form,
        ]);
    }

/*
    #[Route('/{id}/show', name: 'pantry_show', methods: ['GET'])]
    public function show(Pantry $pantry): Response
    {
        $this->denyAccessUnlessGranted('manage', $pantry);
        return $this->render('pantry/show.html.twig', [
            'pantry' => $pantry,
        ]);
    }
*/
    #[Route('/{id}/edit', name: 'pantry_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Pantry $pantry, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('manage', $pantry);
        $form = $this->createForm(PantryType::class, $pantry, ['user' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('pantry/edit.html.twig', [
            'pantry' => $pantry,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'pantry_delete', methods: ['POST'])]
    public function delete(Request $request, Pantry $pantry, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('manage', $pantry);
        if ($this->isCsrfTokenValid('delete'.$pantry->getId(), $request->request->get('_token'))) {
            $entityManager->remove($pantry);
            $entityManager->flush();
        }

        return $this->redirectToRoute('house_index', [], Response::HTTP_SEE_OTHER);
    }
}
