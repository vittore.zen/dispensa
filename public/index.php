<?php

use App\Kernel;

require_once dirname(__DIR__).'/vendor/autoload_runtime.php';

return function (array $context) {
    if ($_SERVER['APP_ENV'] !== 'prod') {
        require dirname(__DIR__) . '/c3.php';
    }

    return new Kernel($context['APP_ENV'], (bool) $context['APP_DEBUG']);
};



